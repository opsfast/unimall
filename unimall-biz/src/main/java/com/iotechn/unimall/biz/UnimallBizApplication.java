package com.iotechn.unimall.biz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnimallBizApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnimallBizApplication.class, args);
	}

}
